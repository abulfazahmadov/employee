const Department = require('../model/department');

module.exports.getAllDepartments = (req, res)=>{
	
    Department.findAll()
	.then((departments)=>{
		res.send(departments);
	})
	.catch((err)=> {
	    res.send(err);
	});
};

module.exports.getDepartmentById = (req, res)=>{
	
    Department.findAll({
		 where: {
			 id:req.params.departmentId
       }
	})
	.then((departments)=>{
		res.send(departments);
	})
	.catch((err)=> {
	    res.send(err);
	});
};