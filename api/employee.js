const Employee = require('../model/employee');
const Department = require('../model/department');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports.getAllEmployees = (req, res)=>{
	
    Employee.findAll({		
	include: [{
    model: Department
   }]
	})
	.then((employees)=>{
		let empList = [];
		
		for(let i=0; i < employees.length; ++i){
			employee={
				id: employees[i].empId,
				name: employees[i].empName,
				active: employees[i].empActive,
				name: employees[i].empName,
				department: employees[i].department,
			};
			empList.push(employee);
		}
		
		res.send(empList);
	})
	.catch((err)=> {
	    res.send(err);
	});
};

module.exports.searchEmployeesAndPaginate = (req, res)=>{
	let startsWith = req.query.name+'%';
	let offset;
	let limit = 10;
	let page = req.query.page;
	
	Employee.findAndCountAll()
    .then((data) => {
	 
	 let pages = Math.ceil(data.count / limit);
		offset = limit * (page - 1);
		
    Employee.findAll({ 
	where:{
		empName: {[Op.like]: startsWith },
	},
	limit: limit,
    offset: offset,
	})
	.then((employees)=>{
		let empList = [];
		
		for(let i=0; i < employees.length; ++i){
			employee={
				id: employees[i].empId,
				name: employees[i].empName,
				active: employees[i].empActive,
				name: employees[i].empName,
				department: employees[i].department,
			};
			empList.push(employee);
		}
		
		res.send(empList);
	})
	.catch((err)=> {
	    res.send(err);
	});
	})
	.catch((err)=> {
	    res.send(err);
	});;
};

module.exports.getEmployeeById = (req, res)=>{
	
    Employee.findAll({
		 where: {
			 empId:req.params.employeeId
       }
	})
	.then((employees)=>{
		res.send(employees);
	})
	.catch((err)=> {
	    res.send(err);
	});
};

module.exports.createEmployee = (req, res)=>{
	
    Employee.create(req.body)
	.then((employee)=>{
		res.send(employee);
	})
	.catch((err)=> {
	    res.send(err);
	});
};


module.exports.updateEmployeeById = (req, res)=>{
	
    Employee.update(req.body,
	{
		where:{
			empId: parseInt(req.body.empId)
		}
	})
	.then((employee)=>{
		res.send({response: "Updated successfully"});
	})
	.catch((err)=> {
	    res.send(err);
	});
};

module.exports.deleteEmployeeById = (req, res)=>{
	
    Employee.destroy({
		where:{
			empId: parseInt(req.params.employeeId)
		}
	})
	.then((statusCode)=>{
		if (statusCode >= 100 && statusCode < 600)
         res.status(statusCode).send({response: "delete error"});
        else
         res.send({response: "Deleted successfully"});
	})
	.catch((err)=> {
	    res.send(err);
	});
};

