const Sequelize = require('sequelize');
const sequelizeSettings = require('../sequelize');
const Department =  require('./department');

const sequelize = sequelizeSettings.sequelize;

const Employee = sequelize.define('employees', {
  empId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
  empName: Sequelize.STRING,
  empActive: Sequelize.BOOLEAN,
  
});

Employee.belongsTo(Department);

module.exports = Employee;