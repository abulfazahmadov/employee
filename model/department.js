const Sequelize = require('sequelize');
const sequelizeSettings = require('../sequelize');
const sequelize = sequelizeSettings.sequelize;

const Department = sequelize.define('departments', {
  id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
  name: Sequelize.STRING,
});

module.exports = Department;