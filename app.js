const express = require('express')
const departmentApi = require('./api/department');
const employeeApi = require('./api/employee');
const bodyParser = require('body-parser');


const app = express()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());



app.get('/', (req, res) => res.send('Hello World!'))
app.get('/getAllDepartments', departmentApi.getAllDepartments)
app.get('/getDepartmentById/:departmentId', departmentApi.getDepartmentById)

app.get('/searchEmployee', employeeApi.searchEmployeesAndPaginate)

app.get('/getAllEmployees', employeeApi.getAllEmployees)
app.get('/getEmployeeById/:employeeId', employeeApi.getEmployeeById)
app.post('/createEmployee', employeeApi.createEmployee)
app.get('/deleteEmployeeById/:employeeId', employeeApi.deleteEmployeeById)
app.put('/updateEmployeeById', employeeApi.updateEmployeeById)


app.listen(3000, () => console.log('Example app listening on port 3000!'))