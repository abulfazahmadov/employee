const Sequelize = require('sequelize');
const sequelize = new Sequelize('employee', 'root', 'root', {
  host: 'localhost',
  port: '3309',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  
  operatorsAliases: false
});

module.exports.sequelize = sequelize;

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

/*const Department = require('./model/department');

const Employee = require('./model/employee');

sequelize.sync()
  .then(() => Department.create({
    id: 8,
    name: "1st department"
  }))
  .then(dep => {
    console.log(dep.toJSON());
  });*/